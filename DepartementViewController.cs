﻿using System;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using Foundation;
using System.Linq;
using System.Collections.Generic;
using DataStructures;
using System.Threading.Tasks;

namespace HackZurich
{
	public class DepartmentViewController : UIViewController, IUnassignedTaskHandler
	{
		//base view separation
		private UIScrollView _leftBar;
		private UIView _centerContent;
		private UIView _rightBar;

		//right content stuff
		private UIStatsView _stats;
		private UIView stats;
		private UIScrollView _openTasks;

		//center content stuff
		private UITextField _bottomTextEnterBar;
		private nfloat _chatBarHeight = 30;
		private UIButton _sendMessage;

		private nfloat _taskHeight = 130;

		private nfloat _listCellHeight = 50;

		private UIImageView background;

		//Bring Circles To Front
		private UIView blackShadow;

		//private UIPathView pathView;
		private RootViewController _rootController;

		private UITheCircleGrid _gridOne;
		private UITheCircleGrid _gridTwo;

		private UIButton _closeButton;

		private UIChatBox _box;

		private UILabel _title;

		private UIImageView fade;



		public DepartmentViewController (RootViewController rootController)
		{
			_rootController = rootController;
			_closeButton = new UIButton (new CGRect (0, 0, UIScreen.MainScreen.Bounds.Height * 0.07, UIScreen.MainScreen.Bounds.Height * 0.07));
			_closeButton.SetImage(UIImage.FromFile("close@2x.png"), UIControlState.Normal);
			_closeButton.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width * 0.95, _closeButton.Bounds.Height * 0.8);
			_closeButton.TouchUpInside += (object sender, EventArgs e) => {
				_gridOne.activateDelegationStation.Alpha = 0.0f;
				Console.WriteLine("asdasdsads");
				HideCompanyCircles("adfsa");
			};


			blackShadow = new UIView (UIScreen.MainScreen.Bounds);
			View.Add (_closeButton);
			blackShadow.BackgroundColor = UIColor.Black;
			blackShadow.Alpha = 0.0f;
			Add (blackShadow);

		

			background = new UIImageView (UIScreen.MainScreen.Bounds);
			background.Image = UIImage.FromFile("back.png");
			Add (background); 

			_leftBar = new UIScrollView(new CGRect(0, 85, UIScreen.MainScreen.Bounds.Width * 1.5/8, UIScreen.MainScreen.Bounds.Height - 85));
			_leftBar.BackgroundColor = UIColor.Clear;


			//Content Bar
			_centerContent = new UIView(new CGRect(UIScreen.MainScreen.Bounds.Width * 1.5/8, 0, UIScreen.MainScreen.Bounds.Width * 4/8, UIScreen.MainScreen.Bounds.Height));
			_centerContent.BackgroundColor = UIColor.Clear;


			_bottomTextEnterBar = new UITextField (new CGRect(_centerContent.Bounds.Width * 0.03, _centerContent.Bounds.Height - (_chatBarHeight + _centerContent.Bounds.Width * 0.02), _centerContent.Bounds.Width * 5/6 - (_centerContent.Bounds.Width * 0.03), _chatBarHeight)); 
			_bottomTextEnterBar.Layer.CornerRadius = _chatBarHeight / 2;
			_bottomTextEnterBar.BackgroundColor = UIColor.Clear;
			_bottomTextEnterBar.Layer.BorderColor = UIColor.FromRGB (20, 80, 89).CGColor;
			_bottomTextEnterBar.Layer.BorderWidth = 1;
			_bottomTextEnterBar.Text = "    ";

			_bottomTextEnterBar.EditingDidBegin += (object sender, EventArgs e) => {
				UIView.Animate(0.2, () => {
					_bottomTextEnterBar.Center = new CGPoint(_bottomTextEnterBar.Center.X, UIScreen.MainScreen.Bounds.Height * 0.45);
					_sendMessage.Center = new CGPoint(_sendMessage.Center.X, UIScreen.MainScreen.Bounds.Height * 0.45);
				});
			};
			_bottomTextEnterBar.EditingDidEnd += (object sender, EventArgs e) => {
				_bottomTextEnterBar.Center = new CGPoint(_bottomTextEnterBar.Center.X, UIScreen.MainScreen.Bounds.Height * 0.968);
				_sendMessage.Center = new CGPoint(_sendMessage.Center.X, UIScreen.MainScreen.Bounds.Height * 0.968);
			};
			_centerContent.Add (_bottomTextEnterBar);



			_sendMessage = new UIButton (new CGRect ((nfloat) _bottomTextEnterBar.Bounds.Width + _bottomTextEnterBar.Bounds.X + _centerContent.Bounds.Width * 0.06, _centerContent.Bounds.Height - (_chatBarHeight + _centerContent.Bounds.Width * 0.02), (nfloat) (_centerContent.Bounds.Width * 1/6) - _centerContent.Bounds.Width * 0.03, (nfloat) _chatBarHeight));
			_sendMessage.BackgroundColor = UIColor.FromRGB (20, 80, 89);
			_sendMessage.Layer.CornerRadius = _chatBarHeight / 2;
			_sendMessage.Layer.BorderColor = UIColor.White.CGColor;
			_sendMessage.Layer.BorderWidth = 1;
			_sendMessage.SetTitle ("Send", UIControlState.Normal);
			_sendMessage.SetTitleColor (UIColor.FromRGB(54, 219, 244), UIControlState.Normal);

			_sendMessage.TouchUpInside += (object sender, EventArgs e) => {
				_bottomTextEnterBar.ResignFirstResponder();
				currentGroup.Chat.NewMessage -= ChatRegister;
				currentGroup.Chat.NewMessage += ChatRegister;
				currentGroup.Chat.SimulateChat(currentGroup.Chat, _bottomTextEnterBar.Text.Substring(4));
				_bottomTextEnterBar.Text = "    ";
			};
			_centerContent.Add (_sendMessage);

			_bottomTextEnterBar.BackgroundColor = UIColor.White;
			//Right Bar
			_rightBar = new UIView(new CGRect(UIScreen.MainScreen.Bounds.Width * 5.5/8, 0, UIScreen.MainScreen.Bounds.Width * 2.5/8, UIScreen.MainScreen.Bounds.Height));
			_rightBar.BackgroundColor = UIColor.Clear;

			stats = new UIView(new CGRect(0, 0, _rightBar.Bounds.Width, _rightBar.Bounds.Height * 1/3));
			stats.BackgroundColor = UIColor.Clear;

			_openTasks = new UIScrollView(new CGRect(0, _rightBar.Bounds.Height * 1/3, _rightBar.Bounds.Width, _rightBar.Bounds.Height * 2/3));
			_openTasks.BackgroundColor = UIColor.Clear;



			Add (_openTasks);

			fade = new UIImageView (new CGRect (0, 0, 750, 170));
			fade.Image = UIImage.FromFile ("fade@2x.png");
			fade.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height * 0.4);
			View.Add (fade);

			_rightBar.Add (_openTasks);
			_rightBar.Add (stats);

			UIImageView leftBack = new UIImageView (new CGRect (0, 0, _leftBar.Bounds.Width + 35, UIScreen.MainScreen.Bounds.Height));
			leftBack.Image = UIImage.FromFile ("overlay");
			Add (leftBack);

			View.AddSubviews (_leftBar, _centerContent, _rightBar);

			UIView navigationButtons = new UIView (new CGRect (0, 15, UIScreen.MainScreen.Bounds.Width * 1.5 / 8, 70));
			float imageWidth = (float) navigationButtons.Frame.Width / 4 < 70 ? (float) navigationButtons.Frame.Width / 4 : 70;

			UIButton profileButton = new UIButton (new CGRect (0, 0, imageWidth, imageWidth));
			UIImageView profileImage = new UIImageView (profileButton.Frame);
			profileImage.Image = UIImage.FromBundle ("Profile.png");
			profileButton.AddSubview (profileImage);
			profileButton.TouchUpInside += (sender, e) => _rootController.SwitchToProfileController();
			navigationButtons.AddSubview (profileButton);

			UIButton circlesButton = new UIButton (new CGRect (imageWidth, 0, imageWidth, imageWidth));
			UIImageView circlesImage = new UIImageView (profileButton.Frame);
			circlesImage.Image = UIImage.FromBundle ("Circles.png");
			circlesButton.AddSubview (circlesImage);
			navigationButtons.AddSubview (circlesButton);

			UIButton tasksButton = new UIButton (new CGRect (2 * imageWidth, 0, imageWidth, imageWidth));
			UIImageView tasksImage = new UIImageView (profileButton.Frame);
			tasksImage.Image = UIImage.FromBundle ("Tasks.png");
			tasksButton.AddSubview (tasksImage);
			tasksButton.TouchUpInside += (sender, e) => _rootController.SwitchToTaskController();
			navigationButtons.AddSubview (tasksButton);

			UIButton companyButton = new UIButton (new CGRect (3 * imageWidth, 0, imageWidth, imageWidth));
			UIImageView companyImage = new UIImageView (profileButton.Frame);
			companyImage.Image = UIImage.FromBundle ("Company.png");
			companyButton.AddSubview (companyImage);
			navigationButtons.AddSubview (companyButton);
			companyButton.TouchUpInside += (object sender, EventArgs e) => {DisplayCompanyCircles(UIMock.Instance.Company.Root);};
			View.AddSubview (navigationButtons);

			UIView seperator = new UIView (new CGRect(0, 80, navigationButtons.Bounds.Width, 1));
			seperator.BackgroundColor = UIColor.Black;
			Add (seperator);





			InjectDepartments (UIMock.Instance.Me.MyGroups.ToArray());//groupNames.ToArray());
			View.BackgroundColor = UIColor.White;

			_gridOne = new UITheCircleGrid (UIScreen.MainScreen.Bounds, this);
			Add (_gridOne);
			_gridOne.Alpha = 0.0f;

			_gridTwo = new UITheCircleGrid (new CGRect(_leftBar.Bounds.Width, 0, UIScreen.MainScreen.Bounds.Width - _leftBar.Bounds.Width, UIScreen.MainScreen.Bounds.Height), this);
			Add (_gridTwo);
			_gridTwo.Alpha = 0.0f;

			_stats = new UIStatsView (new CGRect (0, UIScreen.MainScreen.Bounds.Width * 0.03, _rightBar.Bounds.Width * 0.9, 176 + 2 * 44), UIColor.FromRGB (20, 80, 89));
			_stats.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width - (stats.Bounds.Width * 0.96/ 2), stats.Bounds.Height / 2.1);
			Add (_stats);

			_title = new UILabel (new CGRect(0, 0, _centerContent.Bounds.Width, UIScreen.MainScreen.Bounds.Height * 0.07));
			_title.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width * 0.45, _title.Bounds.Height * 0.8);
			_title.Text = "iOS Frontend";
			_title.TextColor = UIColor.FromRGB (20, 80, 89);
			_title.Font = UIFont.BoldSystemFontOfSize (UIFont.LabelFontSize);
			_title.TextAlignment = UITextAlignment.Center;
			View.BringSubviewToFront (_sendMessage);
			Add (_title);


			_box = new UIChatBox (new CGRect(_centerContent.Frame.X + 20, _centerContent.Frame.Y, _centerContent.Frame.Width - 20, _centerContent.Frame.Height - 50));
			View.AddSubview (_box);


			//DisplayCompanyCircles (UIMock.Instance.Company.Root);
			//View.BringSubviewToFront (pathView);
			View.BringSubviewToFront (_leftBar);
			HideCompanyCircles ("");

			ReloadChatData (UIMock.Instance.Me.MyGroups.First().Chat);

			UpdateToGroup (UIMock.Instance.Me.MyGroups.ToArray()[0]);
		}

		private void ChatRegister (object sender, Message m) {
			InvokeOnMainThread(() => {
				if (m.Sender.ID == UIMock.Instance.Me.ID) {
					_box.AddBubble(m.Content, true);
				} else {
					_box.AddBubble(m.Content, false);
				}
					
			});

		}

		private void ReloadChatData (Chat chats) {
			_box.Flush ();
			foreach (var message in chats.Messages) {
				_box.AddBubble (message.Content, message.Sender.ID == UIMock.Instance.Me.ID);
			}
		}

		public override bool PrefersStatusBarHidden ()
		{
			return true;
		}

		private void ReloadOpenTasks (List<Job> jobs) {
			foreach (UIView view in _openTasks.Subviews) {
				view.RemoveFromSuperview ();
			}
			_openTasks.ContentSize = new CGSize(_openTasks.Bounds.Width, UIScreen.MainScreen.Bounds.Height * 0.05 + jobs.Count * (_taskHeight + _centerContent.Bounds.Width * 0.05) - _centerContent.Bounds.Width * 0.05);
			_openTasks.ScrollEnabled = true;
			int currentTaskIndex = 0;
			foreach (var job in jobs) {
				if (job.AssignedTo.Group == currentGroup) {
					UIUnassignedTaskView task = new UIUnassignedTaskView (new CGRect (0.05 * _openTasks.Bounds.Width, UIScreen.MainScreen.Bounds.Height * 0.1 + 0.05 * _openTasks.Bounds.Width + (currentTaskIndex * (0.025 * _openTasks.Bounds.Width + _taskHeight)), 0.9 * _openTasks.Bounds.Width, _taskHeight), job.Title, this, job); 
					task.Alpha = 0.0f;
					_openTasks.Add (task);
					UIView.Animate(0.7, () => {
						task.Alpha = 1.0f;
					});
					currentTaskIndex++;
				}

			}
			View.BringSubviewToFront (fade);
			View.BringSubviewToFront (_stats);
			View.BringSubviewToFront (_box);
			View.BringSubviewToFront (_centerContent);
			View.BringSubviewToFront (_sendMessage);
			View.BringSubviewToFront (blackShadow);
			//View.BringSubviewToFront (pathView);
			View.BringSubviewToFront (_closeButton);
			View.BringSubviewToFront (_gridOne);


		}

		private List<UICircleListElement> elements = new List<UICircleListElement>();
					private Group currentGroup;
		private void InjectDepartments (Group[] groups) {
			_leftBar.ContentSize = new CGSize(_leftBar.Bounds.Width,  10 + 0.01 * _openTasks.Bounds.Width + (0.02 * _openTasks.Bounds.Width + _listCellHeight) * groups.Length);
			//_leftBar.ScrollEnabled = true;
			int currentDepartmentIndex = 0;
			foreach (Group AGroup in groups) {
				
				UICircleListElement listElem = new UICircleListElement (new CGRect (0.02 * _openTasks.Bounds.Width, 10 + 0.01 * _openTasks.Bounds.Width + (0.02 * _openTasks.Bounds.Width + _listCellHeight) * currentDepartmentIndex, _leftBar.Bounds.Width, _listCellHeight), null, groups [currentDepartmentIndex].Name, AGroup, this);
				elements.Add (listElem);
				_leftBar.Add (listElem);
				listElem.TouchUpInside += (object sender, EventArgs e) => {
					currentGroup = listElem._g;
					UpdateToGroup (listElem._g);
					listElem.SetSelected (true);
					foreach (var listeElementtt in elements) {
						if (listeElementtt._g.ID != listElem._g.ID) {
							listeElementtt.SetSelected (false);
						}
					}
				};
				if (currentDepartmentIndex == 0) {
					listElem.SetSelected (true);
					currentGroup = listElem._g;
				}
				currentDepartmentIndex++;
			}

		}

		private void UpdateToGroup (Group g) {
			_stats.ReloadData (g);
			_title.Text = g.Name;
			ReloadChatData (g.Chat);
			InsertJobs (g);
		}

		bool firstStart = true;
		private async void InsertJobs (Group g) {
			if (firstStart) {
				NSTimer timer = NSTimer.CreateScheduledTimer (2, (asdf) => {
					InvokeOnMainThread (() => {
						ReloadOpenTasks (g.Jobs);
						firstStart = false;
					});
				});
			} else {
				ReloadOpenTasks (g.Jobs);
			}
		}

		#region IUnassignedTaskHandler implementation

		public void TaskAccepted (Job j)
		{
			Group prevGroup = j.AssignedTo.Group;
			UIAlertView alert = new UIAlertView ("Accept Task", "Are you sure you want to accept the task " + j.Title + "?", null, "Cancel", "Accept");
			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				if (e.ButtonIndex == 1) {
					UIMock.Instance.Me.AcceptTaskResponsibility (j);
					ReloadOpenTasks (prevGroup.Jobs);
				}
			};
			alert.Show ();

		}

		public void TaskAddCC (Job taskId)
		{
			//throw new NotImplementedException ();
		}

		public void TaskDelegate (Job taskId)
		{
			_gridOne.delegationJob = taskId;
			_gridOne.activateDelegationStation.Alpha = 1.0f;
			DisplayCompanyCircles (UIMock.Instance.Company.Root);
		}

		int circles = 1;
		Random r = new Random();
		public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
		}
		public void DisplayCompanyCircles (INode node) {
			if (!(node is Person)) {
				_gridOne.ResetAllCircles ();
			}
			View.BringSubviewToFront(blackShadow);

			UIView.Animate(0.2, () => {
				blackShadow.Alpha = 0.80f;
				_closeButton.Alpha = 0.8f;
			});

			UIView.Animate (0.3, () => {
				_gridOne.Alpha = 1.0f;
				//pathView.Alpha = 1.0f;
			});
			//View.BringSubviewToFront (pathView);
			View.BringSubviewToFront (_gridOne);
			View.BringSubviewToFront (_closeButton);
			_gridOne.SetCircles (node.Children.Values.ToList (), false);
		}

		public void HideCompanyCircles (string node) {
			//View.BringSubviewToFront(blackhadow);
			UIView.Animate(0.2, () => {
				blackShadow.Alpha = 0.0f;
			});
			UIView.Animate(0.1, () => {
				_closeButton.Alpha = 0.0f;
				_gridOne.Alpha = 0.0f;
				//pathView.Alpha = 0.0f;
			});
			View.SendSubviewToBack (blackShadow);
//			View.SendSubviewToBack (pathView);
			View.SendSubviewToBack (_gridOne);

			_gridOne.ResetPathView ();
		}

		public void AddPathViewItem(INode node) {
			_gridOne.AddNode (node);
		}

		public void SetListWay (List<INode> nodes) {
			//pathView.SetList (nodes);
		}

		public void ResetAllCircles() {
			_gridOne.ResetAllCircles ();
		}

		public void ReloadRightStuff() {
			ReloadOpenTasks (currentGroup.Jobs);
		}
		#endregion
	}
}

