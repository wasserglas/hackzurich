﻿using System;
using UIKit;
using DataStructures;

namespace HackZurich
{
	public class RootViewController : UINavigationController
	{
		private DepartmentViewController _depController;
		private TaskViewController _taskController;
		private ProfileViewController _profileController;

		public RootViewController ()
		{
			SetNavigationBarHidden (true, false);
			_depController = new DepartmentViewController (this);
			AddChildViewController (_depController);
		}

		public void SwitchToTaskController() {
			if (_taskController == null) {
				_taskController = new TaskViewController (this);
			}
			PresentViewController (_taskController, true, () => {});
		}

		public void SwitchToDepartmentController() {
			PopViewController (false);
		}

		public void SendJobToDelegate (Job job) {
			_depController.TaskDelegate (job);
		}
		public void SwitchToProfileController() {
			if (_profileController == null) {
				_profileController = new ProfileViewController (this);
			}
			PresentViewController (_profileController, true, () => {});
		}

		public override bool PrefersStatusBarHidden ()
		{
			return true;
		}
	}
}

