﻿using System;
using DataStructures;

namespace HackZurich
{
	public interface IUnassignedTaskHandler
	{
		void TaskAccepted (Job j);
		void TaskAddCC (Job taskId);
		void TaskDelegate (Job taskId);
	}
}

