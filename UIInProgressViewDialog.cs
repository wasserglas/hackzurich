﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using Foundation;
using DataStructures;

namespace HackZurich
{
	public class UIInProgressViewDialog : TaskDialog
	{
		CGPoint old;

		public UIInProgressViewDialog (CGRect rect, Job task, TaskViewController c) : base (rect, task)
		{
			old = Center;
			AddButton (UIImage.FromBundle ("approval@2x.png"), "Finish", () => {
				UIView.Animate(0.5, () => {
					this.Center = new CGPoint(Superview.Bounds.Width / 2, - UIScreen.MainScreen.Bounds.Height);
				}, () => {c.DisposeBlur();});
			});
			AddButton (UIImage.FromBundle ("Profile.png"), "Add CC", () => {
			});
			AddButton (UIImage.FromBundle ("Circles.png"), "Delegate", () => {
				
			});
				
			UITextView text = new UITextView (new CGRect (0, 0, rect.Width * 0.95, rect.Height * 0.68));
			text.Center = new CGPoint (rect.Width / 2, rect.Height * 0.63);
			text.BackgroundColor = UIColor.White;
			text.Alpha = 0.5f;
			text.Font = UIFont.SystemFontOfSize (UIFont.LabelFontSize);
			text.Layer.CornerRadius = 7f;
			text.Text = task.Title;
			Add (text);


			text.Started += (object sender, EventArgs e) => {
				UIView.Animate(0.2, () => {
					this.Center = new CGPoint(Superview.Bounds.Width / 2, old.Y - UIScreen.MainScreen.Bounds.Height * 0.07);
				});
			};

			text.Ended += (object sender, EventArgs e) => {
				UIView.Animate(0.2, () => {
					this.Center = old;
				});
			};

		}

	}
}

