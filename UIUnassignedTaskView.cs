﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using DataStructures;

namespace HackZurich
{
	public class UIUnassignedTaskView : TaskView
	{
		IUnassignedTaskHandler _handler;
		private Job _job;

		public UIUnassignedTaskView (CGRect rect, string task, IUnassignedTaskHandler handler, Job theJob) : base(rect, theJob, UIBlurEffectStyle.Dark) {
			_handler = handler;
			_job = theJob;
		}

		protected override void ExtendView() {
			nfloat part = _width / 3f;
			nfloat buttonHeight = 30;

			UIButton acceptButton = new UIButton (new CGRect(0, _height - buttonHeight, part, buttonHeight));
			acceptButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			acceptButton.SetTitle ("accept", UIControlState.Normal);
			acceptButton.SetTitleColor (UIColor.Blue, UIControlState.Normal);
			acceptButton.TouchUpInside += (sender, e) => {
				_handler.TaskAccepted (_job);
			};
			//AddSubview (acceptButton);

			UIButton addCCButton = new UIButton (new CGRect(part, _height - buttonHeight, part, buttonHeight));
			addCCButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			addCCButton.SetTitle ("add CC", UIControlState.Normal);
			addCCButton.SetTitleColor (UIColor.Blue, UIControlState.Normal);
			addCCButton.TouchUpInside += (sender, e) => _handler.TaskAddCC(_task);
			//AddSubview (addCCButton);


			UIButton delegateButton = new UIButton (new CGRect(2 * part, _height - buttonHeight, part, buttonHeight));
			delegateButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			delegateButton.SetTitle ("delegate", UIControlState.Normal);
			delegateButton.SetTitleColor (UIColor.Blue, UIControlState.Normal);
			delegateButton.TouchUpInside += (sender, e) => _handler.TaskDelegate(_task);
			//AddSubview (delegateButton);

			UIView barView = new UIView (new CGRect (0, delegateButton.Frame.Top - 3, _width, 1));
			barView.BackgroundColor = UIColor.White;
			//AddSubview (barView);


			var vibrancy = UIVibrancyEffect.FromBlurEffect (_blur);
			var vibrancyView = new UIVisualEffectView (vibrancy) {
				Frame = new CGRect(0,0, _width, _height)
			};

			vibrancyView.ContentView.Add (addCCButton);
			vibrancyView.ContentView.Add (acceptButton);
			vibrancyView.ContentView.Add (delegateButton);
			vibrancyView.ContentView.Add (barView);
			_blurView.ContentView.Add (vibrancyView);
		}
	}
}

