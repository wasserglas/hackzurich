﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using Foundation;
using DataStructures;

namespace HackZurich
{
	public class UIAssignedViewDialog : TaskDialog
	{



		public UIAssignedViewDialog (CGRect rect, Job task) : base (rect, task)
		{

			AddButton (UIImage.FromBundle ("Tasks.png"), "Accept", () => {
			});
			AddButton (UIImage.FromBundle ("Tasks.png"), "Add CC", () => {
			});
			AddButton (UIImage.FromBundle ("Tasks.png"), "Delegate", () => {
			});

		}
	}
}

