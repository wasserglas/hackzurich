﻿using System;
using UIKit;
using CoreGraphics;
using DataStructures;

namespace HackZurich
{
	public class UICircleListElement : UIButton
	{
		private UIImageView previewIcon;
		private UILabel departmentTitle;
		private DepartmentViewController _parent;
		public Group _g;

		public UICircleListElement (CGRect frame, UIImage itemIcon, string circleName, Group g, DepartmentViewController parent) : base (frame)
		{
			_parent = parent;
			_g = g;
			previewIcon = new UIImageView (new CGRect(frame.Width * 0.02, frame.Height * 0.14, frame.Height * 0.4, frame.Height * 0.4));
			if (itemIcon != null) {
				previewIcon.Image = itemIcon;
			} else {
				//previewIcon.Image = UIImage.FromFile ("financial21.jpg");
			}
			previewIcon.ClipsToBounds = true;
			departmentTitle = new UILabel (new CGRect (previewIcon.Bounds.Width + frame.Width * 0.07, -8, frame.Width * 0.7, frame.Height));
			departmentTitle.Text = circleName;
			departmentTitle.TextColor = UIColor.FromRGB (20, 80, 89);
			departmentTitle.Font = UIFont.SystemFontOfSize (UIFont.LabelFontSize);
			Add (departmentTitle);

			previewIcon.Layer.CornerRadius = (nfloat) (frame.Height * 0.7) / 2;
			previewIcon.BackgroundColor = UIColor.Clear;
			previewIcon.Layer.BorderWidth = 1;
			previewIcon.Layer.BorderColor = UIColor.FromRGB (20, 80, 89).CGColor;
			Add (previewIcon);
		}

		public void SetSelected (bool isSelected) {
			if (isSelected) {
				departmentTitle.Font = UIFont.BoldSystemFontOfSize (UIFont.LabelFontSize);
				previewIcon.BackgroundColor = UIColor.FromRGB (20, 80, 89);
			} else {
				departmentTitle.Font = UIFont.SystemFontOfSize (UIFont.LabelFontSize);
				previewIcon.BackgroundColor = UIColor.Clear;
			}
		}
	}
}

