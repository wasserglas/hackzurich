﻿using System;
using UIKit;
using CoreGraphics;
using DataStructures;
using System.Drawing;
using Foundation;
using System.Threading.Tasks;
using System.Net.Http;

namespace HackZurich
{
	public class Circle : UIButton
	{
		private string _name = "";
		private INode _node;
		private DepartmentViewController _parent;
		public int indeX;

		public UILabel _nameLabel;
		private UIImageView _blurredAvatar;
		private UIImageView _blurredView;

		private UIVisualEffectView theBLUR;


		public Circle (CGRect frame, INode node, DepartmentViewController parent, int index) : base (frame)
		{
			
			ClipsToBounds = true;
			indeX = index;
			_parent = parent;
			Layer.CornerRadius = frame.Width / 2;
			BackgroundColor = UIColor.White;

			_blurredAvatar = new UIImageView (new CGRect (0, 0, frame.Width, frame.Height));
			_blurredAvatar.UserInteractionEnabled = false;

			_blurredAvatar.Center = new CGPoint (frame.Width / 2, frame.Height / 2);
			AddSubview (_blurredAvatar);
			Alpha = 0.8f;
			Layer.BorderColor = UIColor.White.CGColor;
			Layer.BorderWidth = 1;

			var blur = UIBlurEffect.FromStyle (UIBlurEffectStyle.Light);
			theBLUR = new UIVisualEffectView (blur) {
				Frame = new RectangleF (0f, 0f, (float)frame.Width, (float)frame.Width)
			};
			theBLUR.Alpha = 1.0f;
			theBLUR.UserInteractionEnabled = false;
			theBLUR.Center = new CGPoint (frame.Width / 2, frame.Height / 2);
			AddSubview (theBLUR);

			_nameLabel = new UILabel (new CGRect (0, 0, frame.Width * 0.9, frame.Height));
			_nameLabel.Center = new CGPoint (frame.Width / 2, frame.Height * 0.5);
			_nameLabel.AdjustsFontSizeToFitWidth = true;
			_nameLabel.Lines = 3;
			_nameLabel.TextAlignment = UITextAlignment.Center;
			_nameLabel.TextColor = UIColor.White;
			_nameLabel.UserInteractionEnabled = false;
			AddSubview (_nameLabel);
			UIView avatarView = new UIView (new CGRect (frame.Width / 4, 0, frame.Width / 2, frame.Width / 2));
			avatarView.Layer.CornerRadius = frame.Width / 4;
			avatarView.ClipsToBounds = true;
			avatarView.UserInteractionEnabled = false;

			_blurredView = new UIImageView (new CGRect (0, 0, frame.Width / 2, frame.Width / 2));
			_blurredView.UserInteractionEnabled = false;
			avatarView.AddSubview (_blurredView);

			AddSubview (avatarView);

		}

		public async Task<UIImage> LoadImage (string imageUrl)
		{
			var httpClient = new HttpClient();

			Task<byte[]> contentsTask = httpClient.GetByteArrayAsync (imageUrl);

			// await! control returns to the caller and the task continues to run on another thread
			var contents = await contentsTask;

			// load from bytes
			return UIImage.LoadFromData (NSData.FromArray (contents));
		}

		private async Task FromUrl (string uri, UIImageView view)
		{
			
			UIImage image = await LoadImage (uri);
			InvokeOnMainThread (() => {
				view.Image = image;
			});
		}


		public string GetName ()
		{
			return _name;
		}

		public void SetName (string name, INode node)
		{
			_node = node;
			_name = name;
			_nameLabel.Text = name;
			_blurredAvatar.Image = UIImage.FromFile ("placeholder@2x.png");
			if (node != null && node.ProfilePicture != null) {
				FromUrl(node.ProfilePicture, _blurredAvatar);
			}
		}

		public INode GetNode ()
		{
			return _node;
		}

		UIStatsView stats;
		public void UnBlur ()  {
			UIView.Animate(0.3, () => {
				theBLUR.Alpha = 0.0f;
			});
			if (stats == null) {
				//stats = new UIStatsView (new CGRect (0, 0, Frame.Width, 5 * 44 + 11));
			}
			//_parent.View.AddSubview (stats);
			//_parent.View.BringSubviewToFront (stats);
		}

		public void Blur ()  {
				theBLUR.Alpha = 1.0f;
		}
	}
}

