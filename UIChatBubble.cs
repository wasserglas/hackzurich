﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using System.Drawing;

namespace HackZurich
{
	public class UIChatBubble : UIView
	{
		UILabel _contentLabel;
		public UIChatBubble (CGRect rect, CGRect contentFrame, string content, bool selfIsSender) : base (rect)
		{
			Layer.CornerRadius = 20f;
			_contentLabel = new UILabel (new CGRect(10, 10, contentFrame.Width, contentFrame.Height));
			_contentLabel.Text = content;
			_contentLabel.AdjustsFontSizeToFitWidth = false;
			_contentLabel.Lines = 0;
			BackgroundColor = selfIsSender ? UIColor.FromRGB (20, 80, 89) : UIColor.Clear;
			if (!selfIsSender) {
				Layer.BorderColor = UIColor.FromRGB (20, 80, 89).CGColor;
				Layer.BorderWidth = 1;
				_contentLabel.TextColor = UIColor.FromRGB (20, 80, 89);
			} else {
				_contentLabel.TextColor = UIColor.White;
			}

			AddSubview (_contentLabel);
		}
	}
}

