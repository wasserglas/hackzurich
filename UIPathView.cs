﻿using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using DataStructures;

namespace HackZurich
{
	public class UIPathView : UIView
	{
		int currentIndex = 0;
		private NodeElement[] nodes = new NodeElement[6];
		DepartmentViewController _d;

		public UIPathView (CGRect frame, DepartmentViewController d) : base (frame)
		{
			BackgroundColor = UIColor.Clear;
			_d = d;
			//BackgroundColor = UIColor.Red;
			UserInteractionEnabled = true;
			Frame = frame;

			for (int i = 0; i < nodes.Length; i++) {
				nodes[i] = new NodeElement (new CGRect(0, 0, Frame.Height / 10, Frame.Height / 10), i, d, this);
				nodes[i].Center = new CGPoint(Frame.Width / 2, (i * (Frame.Height / 7 + 3)));
				AddSubview (nodes[i]);
				nodes [i].TouchUpInside -= SetState;
				nodes [i].TouchUpInside += SetState;
			}
			currentIndex = 0;
		}

		public void NodeClicked (int number) {
			_d.ResetAllCircles ();
			_d.DisplayCompanyCircles (nodes [number].GetNode());
			List<INode> parents = nodes [number].GetNode ().Path();
			Reset ();
			foreach (INode node in parents) {
				AddNode (node);
			}

		}

		private void SetState (object sender, EventArgs a) {
			/*
			NodeElement moveTo = ((NodeElement) (sender));
			_d.DisplayCompanyCircles(moveTo.GetNode());
			Console.WriteLine("Jump");
			List<INode> nodes = moveTo.GetNode ().Path ();
			Reset ();
			foreach (INode node in nodes) {
				AddNode (node);
			}*/
		}


		private INode lastNode;
		public void AddNode (INode node) {
			if (currentIndex >= 6 || (lastNode != null && node.ID == lastNode.ID)) {
				return;
			}
			nodes [currentIndex].SetNode (node);
			lastNode = node;
			currentIndex++;

			foreach (NodeElement nodeElement in nodes) {
				BringSubviewToFront (nodeElement);
			}
		}

		public void Reset() {
			foreach (var node in nodes) {
				node.ResetNodeElement ();
			}
			currentIndex = 0;
			lastNode = null;
		}
	}
}

