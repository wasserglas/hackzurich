﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using System.Collections.Generic;
using DataStructures;

namespace HackZurich
{
	public class TaskViewController : UIViewController, IInProgressHandler, IAssignedHandler, IUnassignedTaskHandler
	{

		private RootViewController _rootController;
		private UIScrollView _assignedTasks;
		private UIScrollView _acceptedTasks;
		private UIScrollView _finishedTasks;
		private float _taskHeight = 130;
		private UIButton _closeButton;

		public TaskViewController (RootViewController rootController)
		{
			
			_rootController = rootController;


		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();



			View.BackgroundColor = UIColor.White;

			float part = (float)View.Frame.Width / 3f;

			_assignedTasks = new UIScrollView (new CGRect (0, 70, part, View.Frame.Height - 70));
			UIImageView leftBack = new UIImageView (new CGRect (0, 0, part, View.Frame.Height));
			leftBack.Image = UIImage.FromBundle ("GridTiltLeft.png");
			View.AddSubview (leftBack);
			View.AddSubview (_assignedTasks);

			_acceptedTasks = new UIScrollView (new CGRect (part, 70, part, View.Frame.Height - 70));
			UIImageView midBack = new UIImageView (new CGRect (part, 0, part, View.Frame.Height));
			midBack.Image = UIImage.FromBundle ("FullGradient.png");
			View.AddSubview (midBack);	
			View.AddSubview (_acceptedTasks);

			_finishedTasks = new UIScrollView (new CGRect (2 * part, 70, part, View.Frame.Height - 70));
			UIImageView rightBack = new UIImageView (new CGRect (2 * part, 0, part, View.Frame.Height));
			rightBack.Image = UIImage.FromBundle ("GradTilt.png");
			View.AddSubview (rightBack);	
			View.AddSubview (_finishedTasks);

			UIView navigationButtons = new UIView (new CGRect (0, 0, UIScreen.MainScreen.Bounds.Width * 1.5 / 8, 70));
			float imageWidth = (float) navigationButtons.Frame.Width / 4 < 70 ? (float) navigationButtons.Frame.Width / 4 : 70;

			UIButton profileButton = new UIButton (new CGRect (0, 0, imageWidth, imageWidth));
			UIImageView profileImage = new UIImageView (profileButton.Frame);
			profileImage.Image = UIImage.FromBundle ("Profile.png");
			profileButton.AddSubview (profileImage);
			navigationButtons.AddSubview (profileButton);

			UIButton circlesButton = new UIButton (new CGRect (imageWidth, 0, imageWidth, imageWidth));
			UIImageView circlesImage = new UIImageView (profileButton.Frame);
			circlesImage.Image = UIImage.FromBundle ("Circles.png");
			circlesButton.TouchUpInside += (sender, e) => DismissViewController(true, () => {});
			circlesButton.AddSubview (circlesImage);
			navigationButtons.AddSubview (circlesButton);

			UIButton tasksButton = new UIButton (new CGRect (2 * imageWidth, 0, imageWidth, imageWidth));
			UIImageView tasksImage = new UIImageView (profileButton.Frame);
			tasksImage.Image = UIImage.FromBundle ("Tasks.png");
			tasksButton.AddSubview (tasksImage);
			navigationButtons.AddSubview (tasksButton);

			UIButton companyButton = new UIButton (new CGRect (3 * imageWidth, 0, imageWidth, imageWidth));
			UIImageView companyImage = new UIImageView (profileButton.Frame);
			companyImage.Image = UIImage.FromBundle ("Company.png");
			companyButton.AddSubview (companyImage);
			navigationButtons.AddSubview (companyButton);
			//View.AddSubview (navigationButtons);


			//InjectOpenTasks ("Send Data Storage", "Node.js bug wtf", "Need money for new project", "Task is weird", "More");

			UILabel assignedTitle = new UILabel (new CGRect (_assignedTasks.Frame.X + 20, _assignedTasks.Frame.Y - 20, _assignedTasks.Frame.Width, 20));
			assignedTitle.Text = "Assigned Tasks";
			assignedTitle.Font = UIFont.SystemFontOfSize (20);
			Add (assignedTitle);

			UILabel inProgressTitle = new UILabel (new CGRect (_acceptedTasks.Frame.X + 20, _acceptedTasks.Frame.Y - 20, _acceptedTasks.Frame.Width, 20));
			inProgressTitle.Text = "Accepted Tasks";
			inProgressTitle.Font = UIFont.SystemFontOfSize (20);
			Add (inProgressTitle);

			UILabel finishedTitle = new UILabel (new CGRect (_finishedTasks.Frame.X + 20, _finishedTasks.Frame.Y - 20, _finishedTasks.Frame.Width, 20));
			finishedTitle.Text = "Finished Tasks";
			finishedTitle.Font = UIFont.SystemFontOfSize (20);
			Add (finishedTitle);

			UILabel title = new UILabel (new CGRect(20, 10, UIScreen.MainScreen.Bounds.Width, 30));
			title.Text = "My Tasks";
			title.TextColor = UIColor.FromRGB (20, 80, 89);
			title.Font = UIFont.BoldSystemFontOfSize (25);
			Add (title);

			_closeButton = new UIButton (new CGRect (0, 0, UIScreen.MainScreen.Bounds.Height * 0.07, UIScreen.MainScreen.Bounds.Height * 0.07));
			UIImage image = UIImage.FromFile("close@2x.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate);

			_closeButton.SetImage(image, UIControlState.Normal);
			_closeButton.TintColor = UIColor.FromRGB (20, 80, 89);

			_closeButton.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width * 0.95, _closeButton.Bounds.Height * 0.8);
			_closeButton.TouchUpInside += (object sender, EventArgs e) => {
				DismissViewController(true, null);
			};
			View.Add (_closeButton);

			InjectOpenTasks (UIMock.Instance.Me.AssignedJobs);
			InjectAcceptedTasks (UIMock.Instance.Me.AcceptedJobs);
			InjectFinishedTaks (UIMock.Instance.Me.FinishedJobs);
		}

		private void InjectOpenTasks (List<Job> jobs) {
			_assignedTasks.ContentSize = new CGSize(_assignedTasks.Bounds.Width, jobs.Count * _taskHeight * 1.1);
			_assignedTasks.ScrollEnabled = true;

			foreach (UIView view in _assignedTasks.Subviews) {
				view.RemoveFromSuperview ();
			}

			int currentTaskIndex = 0;
			foreach (var job in jobs) {
				_assignedTasks.Add (new UIAssignedTaskView (new CGRect (0.05 * _assignedTasks.Bounds.Width, 0.05 * _assignedTasks.Bounds.Width + (currentTaskIndex * (0.025 * _assignedTasks.Bounds.Width + _taskHeight)), 0.9 * _assignedTasks.Bounds.Width, _taskHeight), job.Title, this, job)); 
				currentTaskIndex++;
			}
		}

		public void TaskAccepted (Job j)
		{
			UIAlertView alert = new UIAlertView ("Accept Task", "Are you sure you want to accept the task " + j.Title + "?", null, "Cancel", "Accept");
			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				if (e.ButtonIndex == 1) {
					UIMock.Instance.Me.AcceptTaskResponsibility (j);
					InjectOpenTasks (UIMock.Instance.Me.AssignedJobs);
				}
			};
			alert.Show ();
		}

		public void TaskAddCC (Job taskId)
		{
			//throw new NotImplementedException ();
		}

		public void TaskDelegate (Job taskId)
		{
			DismissViewController (true, () => {
			});
			_rootController.SendJobToDelegate (taskId);
		}

		private void InjectAcceptedTasks (List<Job> jobs) {
			_acceptedTasks.ContentSize = new CGSize(_assignedTasks.Bounds.Width, jobs.Count * _taskHeight * 1.1);
			_acceptedTasks.ScrollEnabled = true;

			foreach (UIView view in _acceptedTasks.Subviews) {
				view.RemoveFromSuperview ();
			}

			int currentTaskIndex = 0;
			foreach (var job in jobs) {
				_acceptedTasks.Add (new InProgressTaskView (new CGRect (0.05 * _acceptedTasks.Bounds.Width, 0.05 * _acceptedTasks.Bounds.Width + (currentTaskIndex * (0.025 * _acceptedTasks.Bounds.Width + _taskHeight)), 0.9 * _acceptedTasks.Bounds.Width, _taskHeight), job.Title, this, job)); 
				currentTaskIndex++;
			}
		}

		private void InjectFinishedTaks (List<Job> jobs) {
			_finishedTasks.ContentSize = new CGSize(_assignedTasks.Bounds.Width, jobs.Count * _taskHeight * 1.3);
			_finishedTasks.ScrollEnabled = true;

			foreach (UIView view in _finishedTasks.Subviews) {
				view.RemoveFromSuperview ();
			}

			int currentTaskIndex = 0;
			foreach (var job in jobs) {
				_finishedTasks.Add (new UIFinishedTaskView  (new CGRect (0.05 * _finishedTasks.Bounds.Width, 0.05 * _finishedTasks.Bounds.Width + (currentTaskIndex * (0.025 * _finishedTasks.Bounds.Width + _taskHeight * 1.3)), 1.6 * _finishedTasks.Bounds.Width, _taskHeight * 1.3), job.Title, this, job)); 
				currentTaskIndex++;
			}
		}

		UIBlurEffect blurBlur;
		UIVisualEffectView blurBlurView;
		UIInProgressViewDialog _copi;
		public void InProgressMoreInfo (InProgressTaskView sender, Job task)
		{
			blurBlur = UIBlurEffect.FromStyle (UIBlurEffectStyle.Dark);
			blurBlurView = new UIVisualEffectView (blurBlur) {
				Frame = new RectangleF (0f, 0f, (float)View.Frame.Width, (float)View.Frame.Height)
			};
			blurBlurView.Alpha = 0.0f;
			View.AddSubview (blurBlurView);	
			_copi = new UIInProgressViewDialog(new CGRect (View.Frame.Width / 2 - View.Frame.Width / 3, View.Frame.Height / 2 - View.Frame.Height / 3, View.Frame.Width * 2 / 3 , View.Frame.Height * 2 / 3), task, this);
			_copi.Alpha = 0.0f;

			View.AddSubview (_copi);

			UIButton _closeButton = new UIButton (new CGRect (0, 0, UIScreen.MainScreen.Bounds.Height * 0.07, UIScreen.MainScreen.Bounds.Height * 0.07));
			_closeButton.SetImage(UIImage.FromFile("close@2x.png"), UIControlState.Normal);
			_closeButton.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width * 0.95, _closeButton.Bounds.Height * 0.8);
			_closeButton.TouchUpInside += (object sender1, EventArgs e) => {
				Console.WriteLine("asdasdsads");
				UIView.Animate(0.3, () => {blurBlurView.Alpha = 0.0f; _copi.Alpha = 0.0f; _closeButton.Alpha = 0.0f;}, () => { blurBlurView.RemoveFromSuperview(); _copi.RemoveFromSuperview(); _closeButton.RemoveFromSuperview();});
			};

			View.AddSubview (_closeButton);
			UIView.Animate (0.3, () => {_copi.Alpha = 1.0f; blurBlurView.Alpha = 1.0f;});
		}
		public void TaskFinished (InProgressTaskView sender, Job job)
		{
			UIMock.Instance.Me.FinishTask (job);
			//throw new NotImplementedException ();
		}

		public void TaskDelegate (InProgressTaskView sender, Job job)
		{
			TaskDelegate (job);
		}

		public void TaskAddCC (InProgressTaskView sender, Job job)
		{
			//throw new NotImplementedException ();
		}



		public void AssignedMoreInfo (Job task)
		{
			var blur = UIBlurEffect.FromStyle (UIBlurEffectStyle.Dark);
			var blurView = new UIVisualEffectView (blur) {
				Frame = new RectangleF (0f, 0f, (float)View.Frame.Width, (float)View.Frame.Height)
			};
			blurView.Alpha = 0.0f;
			View.AddSubview (blurView);	
			UIAssignedViewDialog copy = new UIAssignedViewDialog(new CGRect (View.Frame.Width / 2 - View.Frame.Width / 3, View.Frame.Height / 2 - View.Frame.Height / 3, View.Frame.Width * 2 / 3 , View.Frame.Height * 2 / 3), task);
			copy.Alpha = 0.0f;

			View.AddSubview (copy);

			UIButton _closeButton = new UIButton (new CGRect (0, 0, UIScreen.MainScreen.Bounds.Height * 0.07, UIScreen.MainScreen.Bounds.Height * 0.07));
			_closeButton.SetImage(UIImage.FromFile("close@2x.png"), UIControlState.Normal);
			_closeButton.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width * 0.95, _closeButton.Bounds.Height * 0.8);
			_closeButton.TouchUpInside += (object sender1, EventArgs e) => {
				Console.WriteLine("asdasdsads");
				UIView.Animate(0.3, () => {blurView.Alpha = 0.0f; copy.Alpha = 0.0f; _closeButton.Alpha = 0.0f;}, () => { blurView.RemoveFromSuperview(); copy.RemoveFromSuperview(); _closeButton.RemoveFromSuperview();});
			};

			View.AddSubview (_closeButton);
			UIView.Animate (0.3, () => {copy.Alpha = 1.0f; blurView.Alpha = 1.0f;});
		}

		public void DisposeBlur() {
			UIView.Animate(0.3, () => {blurBlurView.Alpha = 0.0f; _copi.Alpha = 0.0f; _closeButton.Alpha = 0.0f;}, () => { blurBlurView.RemoveFromSuperview(); _copi.RemoveFromSuperview(); _closeButton.RemoveFromSuperview();});
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			ReloadData ();
		}

		public void ReloadData () {
			InjectOpenTasks (UIMock.Instance.Me.AssignedJobs);
			InjectAcceptedTasks (UIMock.Instance.Me.AcceptedJobs);
			InjectFinishedTaks (UIMock.Instance.Me.FinishedJobs);
		}


		public override bool PrefersStatusBarHidden ()
		{
			return true;
		}
	}
}

