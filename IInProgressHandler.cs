﻿using System;
using DataStructures;

namespace HackZurich
{
	public interface IInProgressHandler
	{
		void InProgressMoreInfo(InProgressTaskView sender, Job job);
		void TaskFinished(InProgressTaskView sender, Job job);
		void TaskDelegate(InProgressTaskView sender, Job job);
		void TaskAddCC(InProgressTaskView sender, Job job);
	}
}

