﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using Foundation;
using DataStructures;

namespace HackZurich
{
	public class UIAssignedTaskView : TaskView
	{
		TaskViewController _handler;
		private Job _job;
		string _task;
		public UIAssignedTaskView (CGRect rect, string task, TaskViewController handler, Job job) : base (rect, job, UIBlurEffectStyle.Dark)
		{
			_job = job;
			_handler = handler;
			_task = task;
		}

		protected override void ExtendView() {
			nfloat part = _width / 2f;
			nfloat buttonHeight = 30;
			UIButton acceptButton = new UIButton (new CGRect(0, _height - buttonHeight, part, buttonHeight));
			acceptButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			acceptButton.SetTitle ("accept", UIControlState.Normal);
			acceptButton.SetTitleColor (UIColor.Blue, UIControlState.Normal);
			acceptButton.TouchUpInside += (object sender, EventArgs e) => {
				UIAlertView alert = new UIAlertView ("Accept Task", "Are you sure you want to accept the task " + _job.Title + "?", null, "Cancel", "Accept");
				alert.Clicked += (object senderer, UIButtonEventArgs er) => {
					if (er.ButtonIndex == 1) {
						UIMock.Instance.Me.AcceptTaskResponsibility(_job);
						_handler.ReloadData();
					}
				};
				alert.Show ();
			};
			//AddSubview (acceptButton);

			UIButton delegateButton = new UIButton (new CGRect(1 * part, _height - buttonHeight, part, buttonHeight));
			delegateButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			delegateButton.SetTitle ("delegate", UIControlState.Normal);
			delegateButton.SetTitleColor (UIColor.Blue, UIControlState.Normal);
			delegateButton.TouchUpInside += (object sender, EventArgs e) => {
				_handler.TaskDelegate(_job);
			};
			//delegateButton.TouchUpInside += (sender, e) => _titleLabel.Text = "Clicked!";
			//AddSubview (delegateButton);

			UIView buttonBarView = new UIView (new CGRect (0, delegateButton.Frame.Top - 3, _width, 1));
			buttonBarView.BackgroundColor = UIColor.White;
			//AddSubview (buttonBarView);

			var vibrancy = UIVibrancyEffect.FromBlurEffect (_blur);
			var vibrancyView = new UIVisualEffectView (vibrancy) {
				Frame = new CGRect(0,0, _width, _height)
			};

			vibrancyView.ContentView.Add (acceptButton);
			vibrancyView.ContentView.Add (delegateButton);
			vibrancyView.ContentView.Add (buttonBarView);
			_blurView.ContentView.Add (vibrancyView);
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			_handler.AssignedMoreInfo (_job);
		}
	}
}

