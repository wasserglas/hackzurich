﻿using System;
using UIKit;
using CoreGraphics;
using DataStructures;
using System.Collections.Generic;

namespace HackZurich
{
	public class UITheCircleGrid : UIView
	{
		private Circle[] circles;

		private int circleRadius = 150;
		private int totalCircles = 12;

		private DepartmentViewController _parent;
		private UILabel _title;

		public UIButton activateDelegationStation;

		public UIPathView pathView;
		UIStatsView _stats = new UIStatsView (new CGRect (0, 0, UIScreen.MainScreen.Bounds.Width / 3.5, 5 * 44 + 11), UIColor.FromRGB(38, 198, 218));


		public UITheCircleGrid (CGRect frame, DepartmentViewController parent) : base (frame)
		{
			_parent = parent;

			_title = new UILabel (new CGRect(0, 0, frame.Width, UIScreen.MainScreen.Bounds.Height * 0.07));
			_title.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width * 0.5, _title.Bounds.Height * 0.8);
			_title.Text = "Software Development";
			_title.TextColor = UIColor.White;
			_title.Font = UIFont.BoldSystemFontOfSize (UIFont.LabelFontSize);
			_title.TextAlignment = UITextAlignment.Center;
			Add (_title);
			UserInteractionEnabled = true;
			circles = new Circle[totalCircles];
			for (int i = 0; i < totalCircles; i++) {
				circles [i] = new Circle (new CGRect (0, 0, circleRadius, circleRadius), null, _parent, i);
				AddSubview (circles [i]);
				circles [i].Alpha = 0.0f;
			}

			activateDelegationStation = new UIButton (new CGRect(0, 0, circleRadius * 0.3, circleRadius * 0.3));
			activateDelegationStation.BackgroundColor = UIColor.Clear;
			activateDelegationStation.Center = new CGPoint (frame.Width * 0.9, Frame.Height * 0.9);
			activateDelegationStation.TouchUpInside += (object sender, EventArgs e) => {
				ChangeDeligationState ();
			};
			activateDelegationStation.Alpha = 0.0f;
			activateDelegationStation.SetImage (new UIImage ("redo@2x.png"), UIControlState.Normal);
			UILabel delegatee = new UILabel (new CGRect(0, activateDelegationStation.Bounds.Width * 0.9, activateDelegationStation.Bounds.Width * 1.2, 40));
			delegatee.Text = "Assign";
			delegatee.TextColor = UIColor.FromRGB (216, 27, 96);
			activateDelegationStation.AddSubview (delegatee);
			Add (activateDelegationStation);

			pathView = new UIPathView (new CGRect (0, 0, UIScreen.MainScreen.Bounds.Width * 1 / 8, UIScreen.MainScreen.Bounds.Height), parent);
			pathView.Center = new CGPoint (0, UIScreen.MainScreen.Bounds.Height * 0.7);
			Add (pathView); 
			pathView.UserInteractionEnabled = true;
			_stats.Alpha = 0.0f;
			_stats.Center = new CGPoint (UIScreen.MainScreen.Bounds.Width / 2, UIScreen.MainScreen.Bounds.Height * 2.3 / 3);
			Add (_stats);
		}

		private bool HasSet = false;

		public void SetCircles (List<INode> children, bool animate) {
			if (children.Count >= 1 && children [0] != null) {
				if (children [0].Parent.ID == children [0].Parent.Parent.ID) {
					children[0].Parent.Parent.Name = "MyCompany";
					_parent.AddPathViewItem (children[0].Parent.Parent);
				}
			}
			int visibleCircles = children.Count;
			if (children.Count > 0 && children [0] != null) {
				_title.Text = children [0].Parent.Name;
				if (_title.Text == "root") {
					_title.Text = "My Company";
				}
			} else {
				return;
			}
			if (visibleCircles > totalCircles) {
				return;
			}

			int i = -1;
			foreach (Circle c in circles) {
				i++;

				if (isVisible (i, visibleCircles)) {
					UIView.Animate (0.8, () => {
						c.Alpha = 1.0f;
						c.SetName (children [i].Name, children [i]);
						c.Center = new CGPoint(GetPosition (i, children.Count).X + 50, GetPosition (i, children.Count).Y + 30);
						c.TouchUpInside -= Open;
						c.TouchUpInside += Open;
					});
					//Console.WriteLine ("Setting Alpha to 1.0 for circle " + i + " with children: " + c.GetNode().Children.Count);
				} else {
					UIView.Animate (0.3, () => {
						c.Alpha = 0.0f;
						Console.WriteLine ("Setting Alpha to 0.0 for circle " + i);
					});
				}
			}
		}

		private void Open (Object sender, EventArgs e) {
			if (isInDelegationState) {
				if (delegationJob != null) {
					UIAlertView alert = new UIAlertView ("Accept Task", "Are you sure you want to assign the task " + delegationJob.Title + " to " + ((Circle)(sender)).GetNode().Name + "?", null, "Cancel", "Accept");
					alert.Clicked += (object senderr, UIButtonEventArgs er) => {
						if (er.ButtonIndex == 1) {
							((Circle)sender).GetNode ().DelegateTask (delegationJob);
							activateDelegationStation.Alpha = 0.0f;
							ChangeDeligationState ();
							_parent.ReloadRightStuff();
						}
					};
					alert.Show ();

				}
				_parent.HideCompanyCircles ("cole und tobi");
				return;
			}
			if (((Circle)sender).GetNode ().Children.Count == 0 && !(((Circle)sender).GetNode () is Person)) {
				return;
			} else if (((Circle)sender).GetNode () is Person) {
				AnimateInfo (((Circle)sender).indeX);
			}
			_parent.AddPathViewItem (((Circle)sender).GetNode ());
			_parent.DisplayCompanyCircles (((Circle) sender).GetNode());
		}

		private void AnimateInfo (int circleNumber) {
			 //maybe do sth fancy here later
			UIView.Animate(0.3, () => {
				circles [circleNumber].Center = new CGPoint(Frame.Width / 2, Frame.Height / 3.2);
				circles [circleNumber].Alpha  = 1.0f;
				circles [circleNumber].UnBlur();
				circles [circleNumber].Transform = CGAffineTransform.MakeScale(2, 2);
				circles [circleNumber]._nameLabel.Text = "";
				_title.Text = circles [circleNumber].GetName();
			});

			DisplayStats (circles [circleNumber].GetNode ().Stats, true);

			for (int i = 0; i < circles.Length; i++) {
				if (i != circleNumber) {
					UIView.Animate (0.3, () => {
						circles [i].Alpha = 0.0f;
					});
				}
			}
				

		}


		public bool isVisible (int circleNumber, int totalNumberOfCircles) {
			if (circleNumber < totalNumberOfCircles) {
				return true;
			} else {
				return false;
			}
		}

		public CGPoint GetPosition (int circleNumber, int totalNumberOfCircles) {
			if (totalNumberOfCircles == 1) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width / 2, Frame.Height / 2);
				}
			} else if (totalNumberOfCircles == 2) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width / 4, Frame.Height / 2);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 3 / 4, Frame.Height / 2);
				}
			} else if (totalNumberOfCircles == 3) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width / 4, Frame.Height / 3);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 3 / 4, Frame.Height / 3);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width / 2, Frame.Height * 2 / 3);
				}
			} else if (totalNumberOfCircles == 4) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width / 4, Frame.Height / 3);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 3 / 4, Frame.Height / 3);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width / 4, Frame.Height * 2 / 3);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width * 3 / 4, Frame.Height * 2 / 3);
				}
			} else if (totalNumberOfCircles == 5) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width / 4, Frame.Height / 4);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 3 / 4, Frame.Height / 4);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width * 2 / 4, Frame.Height * 2 / 4);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width / 4, Frame.Height * 3 / 4);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width * 3 / 4, Frame.Height * 3 / 4);
				}
			} else if (totalNumberOfCircles == 6) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width * 2 / 7, Frame.Height / 3);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 4 / 7, Frame.Height / 3);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width * 6/ 7, Frame.Height / 3);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width * 1 / 7, Frame.Height * 2 / 3);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width * 3 / 7 , Frame.Height * 2 / 3);
				} else if (circleNumber == 5) {
					return new CGPoint (Frame.Width * 5 / 7, Frame.Height * 2 / 3);
				}
			} else if (totalNumberOfCircles == 7) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width * 2 / 8, Frame.Height / 3);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 4/ 8, Frame.Height / 3);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width * 6 / 8, Frame.Height / 3);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width / 8, Frame.Height * 2 / 3);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width * 3/ 8, Frame.Height * 2/ 3);
				} else if (circleNumber == 5) {
					return new CGPoint (Frame.Width * 5 / 8, Frame.Height * 2/ 3);
				} else if (circleNumber == 6) {
					return new CGPoint (Frame.Width * 7 / 8, Frame.Height * 2/ 3);
				}
			} else if (totalNumberOfCircles == 8) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width * 2 / 6, Frame.Height * 1 / 5);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 4 / 6, Frame.Height * 1 / 5);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width * 1 / 6, Frame.Height * 2 / 5);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width * 3 / 6, Frame.Height * 2 / 5);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width * 5 / 6, Frame.Height * 2 / 5);
				} else if (circleNumber == 5) {
					return new CGPoint (Frame.Width * 2 / 6, Frame.Height * 3 / 5);
				} else if (circleNumber == 6) {
					return new CGPoint (Frame.Width * 4 / 6, Frame.Height * 3 / 5);
				} else if (circleNumber == 7) {
					return new CGPoint (Frame.Width * 3 / 6, Frame.Height * 4 / 5);
				}
			} else if (totalNumberOfCircles == 9) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width * 3 / 6, Frame.Height * 1 / 6);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 2 / 6, Frame.Height * 2 / 6);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width * 4 / 6, Frame.Height * 2 / 6);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width * 1 / 6, Frame.Height * 3 / 6);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width * 3 / 6, Frame.Height * 3 / 6);
				} else if (circleNumber == 5) {
					return new CGPoint (Frame.Width * 5 / 6, Frame.Height * 3 / 6);
				} else if (circleNumber == 6) {
					return new CGPoint (Frame.Width * 2 / 6, Frame.Height * 4 / 6);
				} else if (circleNumber == 7) {
					return new CGPoint (Frame.Width * 4 / 6, Frame.Height * 4 / 6);
				} else if (circleNumber == 8) {
					return new CGPoint (Frame.Width * 3 / 6, Frame.Height * 5 / 6);
				}
			} else if (totalNumberOfCircles == 10) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width *  2 / 8, Frame.Height *  1 / 4);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width *  4 / 8, Frame.Height *  1 / 4);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width *  6 / 8, Frame.Height *  1 / 4);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width *  1 / 8, Frame.Height *  2 / 4);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width *  3 / 8, Frame.Height *  2 / 4);
				} else if (circleNumber == 5) {
					return new CGPoint (Frame.Width *  5 / 8, Frame.Height *  2 / 4);
				} else if (circleNumber == 6) {
					return new CGPoint (Frame.Width *  7 / 8, Frame.Height *  2 / 4);
				} else if (circleNumber == 7) {
					return new CGPoint (Frame.Width *  2 / 8, Frame.Height *  3 / 4);
				} else if (circleNumber == 8) {
					return new CGPoint (Frame.Width *  4 / 8, Frame.Height *  3 / 4);
				} else if (circleNumber == 9) {
					return new CGPoint (Frame.Width *  6 / 8, Frame.Height *  3 / 4);
				}
			} else if (totalNumberOfCircles == 11) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width * 1 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 3 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width * 5 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width * 7 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width * 2 / 8, Frame.Height * 2 / 4);
				} else if (circleNumber == 5) {
					return new CGPoint (Frame.Width * 4 / 8, Frame.Height * 2 / 4);
				} else if (circleNumber == 6) {
					return new CGPoint (Frame.Width * 6 / 8, Frame.Height * 2 / 4);
				} else if (circleNumber == 7) {
					return new CGPoint (Frame.Width * 1 / 8, Frame.Height * 3 / 4);
				} else if (circleNumber == 8) {
					return new CGPoint (Frame.Width * 3 / 8, Frame.Height * 3 / 4);
				} else if (circleNumber == 9) {
					return new CGPoint (Frame.Width * 5 / 8, Frame.Height * 3 / 4);
				} else if (circleNumber == 10) {
					return new CGPoint (Frame.Width * 7 / 8, Frame.Height * 3 / 4);
				}
			} else if (totalNumberOfCircles == 12) {
				if (circleNumber == 0) {
					return new CGPoint (Frame.Width * 1 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 1) {
					return new CGPoint (Frame.Width * 3 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 2) {
					return new CGPoint (Frame.Width * 5 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 3) {
					return new CGPoint (Frame.Width * 7 / 8, Frame.Height * 1 / 4);
				} else if (circleNumber == 4) {
					return new CGPoint (Frame.Width * 1 / 8, Frame.Height * 2 / 4);
				} else if (circleNumber == 5) {
					return new CGPoint (Frame.Width * 3 / 8, Frame.Height * 2 / 4);
				} else if (circleNumber == 6) {
					return new CGPoint (Frame.Width * 5 / 8, Frame.Height * 2 / 4);
				} else if (circleNumber == 7) {
					return new CGPoint (Frame.Width * 7 / 8, Frame.Height * 2 / 4);
				} else if (circleNumber == 8) {
					return new CGPoint (Frame.Width * 1 / 8, Frame.Height * 3 / 4);
				} else if (circleNumber == 9) {
					return new CGPoint (Frame.Width * 3 / 8, Frame.Height * 3 / 4);
				} else if (circleNumber == 10) {
					return new CGPoint (Frame.Width * 5 / 8, Frame.Height * 3 / 4);
				} else if (circleNumber == 11) {
					return new CGPoint (Frame.Width * 7 / 8, Frame.Height * 3 / 4);
				}
			}
			return new CGPoint (0, 0);
		}

		bool isInDelegationState = false;
		public Job delegationJob;
		public void ChangeDeligationState () {
			if (!isInDelegationState) {
				
				foreach (Circle c in circles) {
					c.Layer.BorderWidth = 3;
					c.Layer.BorderColor = UIColor.FromRGB (216, 27, 96).CGColor;
					isInDelegationState = true;
				}
			} else {
				foreach (Circle c in circles) {
					c.Layer.BorderWidth = 1;
					c.Layer.BorderColor = UIColor.White.CGColor;
					c.Transform = CGAffineTransform.MakeScale (1.0f, 1.0f);
					c.Blur ();
					isInDelegationState = false;

				}
			}
		}

		public void ResetAllCircles() {
			foreach (Circle c in circles) {
				c.Layer.BorderWidth = 1;
				c.Layer.BorderColor = UIColor.White.CGColor;
				c.Transform = CGAffineTransform.MakeScale (1.0f, 1.0f);
				c.Blur ();
				isInDelegationState = false;
				DisplayStats (null, false);
			}
		}
		public void ResetPathView () {
			//pathView.Reset ();
		}

		public void AddNode (INode node) {
			pathView.AddNode (node);
		}



		public void DisplayStats(Statistics stats, bool shows) {
			if (shows) {
				_stats.ReloadData (stats);
				_stats.Alpha = 1.0f;

			} else {
				_stats.Alpha = 0.0f;
			}

			//_stats.ReloadData(
		}
	}
}

