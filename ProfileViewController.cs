﻿using System;
using UIKit;

namespace HackZurich
{
	public class ProfileViewController : UIViewController
	{
		RootViewController _rootController;
		public ProfileViewController (RootViewController rootController) 
		{
			_rootController = rootController;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = UIColor.White;
			UIChatBox box = new UIChatBox (View.Frame);
			View.Add (box);

			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", true);
			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", false);
			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", true);
			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", false);
			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", true);
			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", false);
			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", true);
			box.AddBubble ("aoehutnshaeosnuh tnsaoehuntsaoeh  uaoteuh aosetnhu aoesntu haetnsouh tnsaoeh utnsaoehu tnsaoheu eo ahseou", false);
			}
	}
}

