﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using Foundation;
using DataStructures;

namespace HackZurich
{
	public class InProgressTaskView : TaskView 
	{
		private TaskViewController _handler;
		private Job _job;

		public InProgressTaskView (CGRect rect, string task, TaskViewController handler, Job job) : base (rect, job, UIBlurEffectStyle.Light)
		{
			_handler = handler;
			_job = job;
		}

		protected override void ExtendView() {

			var vibrancy = UIVibrancyEffect.FromBlurEffect (_blur);
			var vibrancyView = new UIVisualEffectView (vibrancy) {
				Frame = new CGRect(0,0, _width, _height)
			};

			nfloat part = _width / 2f;
			nfloat buttonHeight = 30;

			ChangeTitleColor (UIColor.Black);
			ChangeDescriptionColor (UIColor.Black);

			UIButton finishButton = new UIButton (new CGRect(0, _height - buttonHeight, part, buttonHeight));
			finishButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			finishButton.SetTitle ("finish", UIControlState.Normal);
			finishButton.SetTitleColor (UIColor.FromRGB(20, 80, 89), UIControlState.Normal);
			finishButton.TouchUpInside += (object sender, EventArgs e) => {
				UIAlertView alert = new UIAlertView ("Finish Task", "Are you sure you want to finish the task " + _job.Title + "?", null, "Cancel", "Finish");
				alert.Clicked += (object senderee, UIButtonEventArgs sde) => {
					if (sde.ButtonIndex == 1) {
						_handler.TaskFinished(this, _job);
						_handler.ReloadData();
					}
				};
				alert.Show ();

			};
			AddSubview (finishButton);

			UIButton delegateButton = new UIButton (new CGRect( part, _height - buttonHeight, part, buttonHeight));
			delegateButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			delegateButton.SetTitle ("delegate", UIControlState.Normal);
			delegateButton.SetTitleColor (UIColor.FromRGB(20, 80, 89), UIControlState.Normal);
			delegateButton.TouchUpInside += (object sender, EventArgs e) => {
				_handler.TaskDelegate(_job);
			};
			AddSubview (delegateButton);

			UIView buttonBarView = new UIView (new CGRect (0, delegateButton.Frame.Top - 3, _width, 1));
			buttonBarView.BackgroundColor = UIColor.Blue;
			vibrancyView.ContentView.Add (buttonBarView);
			_blurView.ContentView.Add (vibrancyView);
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			_handler.InProgressMoreInfo (this, _job);
		}
	}
}

