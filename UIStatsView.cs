﻿using System;
using CoreGraphics;
using UIKit;
using Foundation;
using DataStructures;

namespace HackZurich
{
	public class UIStatsView : UIView
	{

		TableSource s;
		UITableView table;
		UIColor _mainColor;
		public UIStatsView (CGRect frame, UIColor mainColor) : base (frame)
		{
			

			_mainColor = mainColor;
			table = new UITableView (frame, UITableViewStyle.Plain);
			//table.BackgroundView.BackgroundColor = UIColor.Clear;
			s =  new TableSource (_mainColor);
			table.Source = s;
			table.BackgroundColor = UIColor.Clear;
			table.SeparatorColor = UIColor.Clear;
			table.ScrollEnabled = false;
			Add (table);
		}

		public void ReloadData (Group g) {
			table.BeginUpdates ();
			s.ReloadData (g);
			table.ReloadData ();
			table.EndUpdates ();
		}

		public void ReloadData (Statistics stat) {
			table.BeginUpdates ();
			s.ReloadData (stat);
			table.ReloadData ();
			table.EndUpdates ();
		}
	}

	public class TableSource : UITableViewSource 
	{
		string[] TableItems = {"Statistics", "Average Response Time", "Daily Completed Tasks", "Answer Quality", "", "Total Score"};
		string CellIdentifier = "TableCell";
		private UIColor _mainColor;
		public TableSource (UIColor mainColor)
		{
			_mainColor = mainColor;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return TableItems.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			string item = TableItems[indexPath.Row];

			//---- if there are no cells to reuse, create a new one
			if (cell == null)
			{ cell = new UITableViewCell (UITableViewCellStyle.Value1, CellIdentifier); }
			cell.Bounds = new CGRect (0, 0, cell.Bounds.Width + 40, cell.Bounds.Height);
			cell.BackgroundColor = _mainColor;
			cell.Layer.CornerRadius = 7;
			cell.TextLabel.Text = item;
			if (indexPath.Row == 0) {
				cell.Alpha = 0.8f;
				cell.TextLabel.TextColor = UIColor.White;
				cell.TextLabel.Font = UIFont.BoldSystemFontOfSize (22);
			} else if (indexPath.Row == 1) {
				cell.DetailTextLabel.Text = minutes + " mins";
				cell.DetailTextLabel.TextColor = UIColor.FromRGB (233, 30, 99);
				cell.BackgroundColor = UIColor.Clear;
				cell.TextLabel.TextColor = _mainColor;
			} else if (indexPath.Row == 2) {
				cell.DetailTextLabel.Text = averageDailyTasksCompleted.ToString();
				cell.BackgroundColor = UIColor.Clear;
				cell.DetailTextLabel.TextColor = UIColor.FromRGB (188, 255, 3);
				cell.TextLabel.TextColor = _mainColor;
			} else if (indexPath.Row == 3) {
				cell.DetailTextLabel.Text = lifetimeTasks.ToString();
				cell.BackgroundColor = UIColor.Clear;
				cell.TextLabel.TextColor = _mainColor;
				cell.DetailTextLabel.TextColor = UIColor.FromRGB (255, 193, 7);
			} else if (indexPath.Row == 4) {
				cell.BackgroundColor = UIColor.Clear;
				UIView sum = new UIView (new CGRect(0, 5.5, cell.Bounds.Width, 1));
				sum.BackgroundColor = _mainColor;
				cell.Add (sum);
			} else if (indexPath.Row == 5) {
				cell.BackgroundColor = UIColor.Clear;
				cell.TextLabel.TextColor = _mainColor;
				cell.DetailTextLabel.TextColor = UIColor.FromRGB (255, 193, 7);
				cell.DetailTextLabel.Text = total.ToString("0.00");
			}
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			return cell;
		}

		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if (indexPath.Row == 4) {
				return 11;
			} else {
				return 44;
			}
		}

		private int lifetimeTasks = 0;
		private int averageDailyTasksCompleted = 0;
		private int minutes = 0;
		private double total = 0;

		public void ReloadData (Group g) {
			lifetimeTasks = (int) g.Stats.LifetimeTasksCompleted;
			averageDailyTasksCompleted = (int) g.Stats.AverageDailyTasksCompleted;
			minutes = (int) g.Stats.AverageTaskCompletionTime.TotalMinutes;
			total = g.Stats.OverallScore;
			Console.WriteLine ("Liftime: " + lifetimeTasks);
		}

		public void ReloadData (Statistics s) {
			lifetimeTasks = (int) s.LifetimeTasksCompleted;
			averageDailyTasksCompleted = (int) s.AverageDailyTasksCompleted;
			minutes = (int) s.AverageTaskCompletionTime.TotalMinutes;
			total = s.OverallScore;
			Console.WriteLine ("Liftime: " + lifetimeTasks);
		}





	}
}

