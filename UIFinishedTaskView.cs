﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using Foundation;
using DataStructures;

namespace HackZurich
{
	public class UIFinishedTaskView : TaskView
	{
		private Job _job; 
		private TaskViewController taskController;
		public UIFinishedTaskView (CGRect rect, string task, TaskViewController c, Job job ) : base (rect, job, UIBlurEffectStyle.ExtraLight)
		{
			_job = job;
			taskController = c;
		}

		protected override void ExtendView() {
			Alpha = 0.4f;

			ChangeTitleColor (UIColor.Black);
			ChangeDescriptionColor (UIColor.Black);

			UIView creatorBarView = new UIView (new CGRect (0, 2 * _padding + 40 + 3, _width, 1));
			creatorBarView.BackgroundColor = UIColor.Blue;

			UILabel creatorLabel = new UILabel (new CGRect (_padding, creatorBarView.Frame.Bottom + 3, _width - 2 * _padding, 20));
			creatorLabel.Text = "Creator: ";
			creatorLabel.Font = UIFont.BoldSystemFontOfSize (creatorLabel.Font.PointSize);
			AddSubview (creatorLabel);

			NSString nsString = new NSString(creatorLabel.Text);
			UIStringAttributes attribs = new UIStringAttributes { Font = creatorLabel.Font };
			CGSize size = nsString.GetSizeUsingAttributes(attribs);

			UILabel creatorNameLabel = new UILabel (new CGRect (creatorLabel.Frame.X + size.Width, creatorLabel.Frame.Y, Frame.Width - 2 * _padding - size.Width, 20));
			creatorNameLabel.Text = "Rene Brandel";
			AddSubview (creatorNameLabel);

			UILabel ccLabel = new UILabel (new CGRect (_padding, creatorLabel.Frame.Bottom, _width - 2 * _padding, 20));
			ccLabel.Text = "CC: ";
			ccLabel.Font = UIFont.BoldSystemFontOfSize (creatorLabel.Font.PointSize);
			AddSubview (ccLabel);

			nsString = new NSString(ccLabel.Text);
			attribs = new UIStringAttributes { Font = ccLabel.Font };
			size = nsString.GetSizeUsingAttributes(attribs);

			UILabel ccNameLabel = new UILabel (new CGRect (ccLabel.Frame.X + size.Width, ccLabel.Frame.Y, Frame.Width - 2 * _padding - size.Width, 20));
			ccNameLabel.Text = "Cole Bailey, Yue Ou";
			AddSubview (ccNameLabel);

			UILabel assignedLabel = new UILabel (new CGRect (_padding, ccLabel.Frame.Bottom, _width - 2 * _padding, 20));
			assignedLabel.Text = "Assigned to: ";
			assignedLabel.Font = UIFont.BoldSystemFontOfSize (creatorLabel.Font.PointSize);
			AddSubview (assignedLabel);

			nsString = new NSString(assignedLabel.Text);
			attribs = new UIStringAttributes { Font = assignedLabel.Font };
			size = nsString.GetSizeUsingAttributes(attribs);

			UILabel assignedNameLabel = new UILabel (new CGRect (assignedLabel.Frame.X + size.Width, assignedLabel.Frame.Y, Frame.Width - 2 * _padding - size.Width, 20));
			assignedNameLabel.Text = "Tobias Roeddiger";
			AddSubview (assignedNameLabel);


			var vibrancy = UIVibrancyEffect.FromBlurEffect (_blur);
			var vibrancyView = new UIVisualEffectView (vibrancy) {
				Frame = new CGRect(0,0, _width, _height)
			};

			nfloat buttonHeight = 30;

			UIButton deleteButton = new UIButton (new CGRect(0, _height - buttonHeight, _width, buttonHeight));
			deleteButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			deleteButton.SetTitle ("remove", UIControlState.Normal);
			deleteButton.SetTitleColor (UIColor.FromRGB(20, 80, 89), UIControlState.Normal);
			AddSubview (deleteButton);

			UIView buttonBarView = new UIView (new CGRect (0, deleteButton.Frame.Top - 3, _width, 1));
			buttonBarView.BackgroundColor = UIColor.Blue;
			vibrancyView.ContentView.Add (creatorBarView);
			vibrancyView.ContentView.Add (buttonBarView);
			_blurView.ContentView.Add (vibrancyView);
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			UIView.Animate (0.2, () => {
				Alpha = 1.0f;
			});
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			UIView.Animate (0.2, () => {
				Alpha = 0.4f;
			});

		}

		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
			UIView.Animate (0.2, () => {
				Alpha = 0.4f;
			});
		}
	}
}

