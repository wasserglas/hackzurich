﻿using System;
using DataStructures;

namespace HackZurich
{
	public interface IAssignedHandler
	{
		void AssignedMoreInfo(Job task);
	}
}

