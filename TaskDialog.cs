﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using Foundation;
using DataStructures;

namespace HackZurich
{
	public class TaskDialog : UIView
	{


		private nfloat _width;
		private nfloat _height;

		private Job _task;
		private nfloat _padding = 10f;

		private UILabel _titleLabel;
		private UILabel _descLabel;

		private int buttonCount;

		public TaskDialog (CGRect rect, Job task) : base(rect)
		{
			_width = rect.Width;
			_height = rect.Height;
			_task = task;
			InitView ();
			ReloadData (_task);

		}

		private void InitView() {
			Layer.CornerRadius = 7f;
			ClipsToBounds = true;
			var blur = UIBlurEffect.FromStyle (UIBlurEffectStyle.ExtraLight);
			var blurView = new UIVisualEffectView (blur) {
				Frame = new RectangleF (0f, 0f, (float)_width, (float) _height)
			};

			AddSubview (blurView);

			_titleLabel = new UILabel (new CGRect (_padding + 0, _padding + 0, _width - 2 * _padding, 20));
			_titleLabel.Font = UIFont.BoldSystemFontOfSize (20);
			_titleLabel.TextColor = UIColor.Black;
			AddSubview (_titleLabel);

			_descLabel = new UILabel (new CGRect (_padding + 0, _titleLabel.Frame.Bottom, _width - 2 * _padding, 40));
			_descLabel.Font = UIFont.SystemFontOfSize (15);
			_descLabel.TextColor = UIColor.Black;
			AddSubview (_descLabel);

			UIView creatorBarView = new UIView (new CGRect (0, _descLabel.Frame.Bottom + 3, _width, 1));
			creatorBarView.BackgroundColor = UIColor.Blue;

			UILabel creatorLabel = new UILabel (new CGRect (_padding, creatorBarView.Frame.Bottom + 3, _width - 2 * _padding, 20));
			creatorLabel.Text = "Creator: ";
			creatorLabel.Font = UIFont.BoldSystemFontOfSize (creatorLabel.Font.PointSize);
			AddSubview (creatorLabel);

			NSString nsString = new NSString(creatorLabel.Text);
			UIStringAttributes attribs = new UIStringAttributes { Font = creatorLabel.Font };
			CGSize size = nsString.GetSizeUsingAttributes(attribs);

			UILabel creatorNameLabel = new UILabel (new CGRect (creatorLabel.Frame.X + size.Width, creatorLabel.Frame.Y, Frame.Width - 2 * _padding - size.Width, 20));
			creatorNameLabel.Text = "Rene Brandel";
			AddSubview (creatorNameLabel);

			UILabel ccLabel = new UILabel (new CGRect (_padding, creatorLabel.Frame.Bottom, _width - 2 * _padding, 20));
			ccLabel.Text = "CC: ";
			ccLabel.Font = UIFont.BoldSystemFontOfSize (creatorLabel.Font.PointSize);
			AddSubview (ccLabel);

			nsString = new NSString(ccLabel.Text);
			attribs = new UIStringAttributes { Font = ccLabel.Font };
			size = nsString.GetSizeUsingAttributes(attribs);

			UILabel ccNameLabel = new UILabel (new CGRect (ccLabel.Frame.X + size.Width, ccLabel.Frame.Y, Frame.Width - 2 * _padding - size.Width, 20));
			ccNameLabel.Text = "Cole Bailey, Yue Ou";
			AddSubview (ccNameLabel);

			UILabel assignedLabel = new UILabel (new CGRect (_padding, ccLabel.Frame.Bottom, _width - 2 * _padding, 20));
			assignedLabel.Text = "Assigned to: ";
			assignedLabel.Font = UIFont.BoldSystemFontOfSize (creatorLabel.Font.PointSize);
			AddSubview (assignedLabel);

			nsString = new NSString(assignedLabel.Text);
			attribs = new UIStringAttributes { Font = assignedLabel.Font };
			size = nsString.GetSizeUsingAttributes(attribs);

			UILabel assignedNameLabel = new UILabel (new CGRect (assignedLabel.Frame.X + size.Width, assignedLabel.Frame.Y, Frame.Width - 2 * _padding - size.Width, 20));
			assignedNameLabel.Text = "Tobias Roeddiger";
			AddSubview (assignedNameLabel);


			var vibrancy = UIVibrancyEffect.FromBlurEffect (blur);
			var vibrancyView = new UIVisualEffectView (vibrancy) {
				Frame = new CGRect(0,0, _width, _height)
			};

			vibrancyView.ContentView.Add (creatorBarView);
			blurView.ContentView.Add (vibrancyView);
		}

		protected void AddButton(UIImage img, string title, Action action) {
			UIButton button = new UIButton (new CGRect (Frame.Width - (60 * ++buttonCount) - _padding, _padding / 2f, 70, 70));
			UIImageView imgView = new UIImageView (new CGRect (5, 0, 60, 60));
			imgView.Image = img;
			button.AddSubview (imgView);

			UILabel label = new UILabel(new CGRect(5,50,60,10));
			label.Text = title;
			label.Font = UIFont.SystemFontOfSize (10);
			label.TextAlignment = UITextAlignment.Center;
			button.AddSubview (label);

			button.TouchUpInside += (sender, e) => InvokeOnMainThread(action);
			AddSubview (button);
		}

		public void ReloadData(Job task) {

			_titleLabel.Text = task.Title;
			_descLabel.Text = task.Description;
		}
	}
}

