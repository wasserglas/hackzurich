﻿using System;
using UIKit;
using CoreGraphics;
using DataStructures;

namespace HackZurich
{
	public class NodeElement : UIButton
	{
		INode _node;
		public UILabel name;
		public int nodeNumber;
		private DepartmentViewController _parent;

		public NodeElement (CGRect frame, int currentIndex, DepartmentViewController parent, UIPathView view) : base (frame)
		{
			if (currentIndex == 0) {
				Alpha = 1.0f;
			} else {
				Alpha = 0.0f;
			}

			Layer.CornerRadius = frame.Width / 2;
			UIButton button = new UIButton (frame);
			button.TouchUpInside += (object sender, EventArgs e) => {
				view.NodeClicked(nodeNumber);
			};
			Add (button);
			nodeNumber = currentIndex;

			name = new UILabel (new CGRect (frame.Width + 3, 0, frame.Width * 1.4, frame.Height));
			name.TextColor = UIColor.White;
			name.Font = UIFont.SystemFontOfSize (UIFont.LabelFontSize);
			name.Text = "";
			name.Lines = 3;
			//name.BackgroundColor = UIColor.Green;
			AddSubview (name);
		}

		public void SetNode(INode node) {
			Alpha = 1.0f;
			name.Text = node.Name;
			BackgroundColor = UIColor.FromRGB(38, 198, 218);
			_node = node;
		}


		public INode GetNode () {
			return _node;
		}

		public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);

			UITouch touch = touches.AnyObject as UITouch;
		}

		public override void TouchesEnded (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
		}

		public void ResetNodeElement () {
			if (nodeNumber == 0) {
				BackgroundColor = UIColor.FromRGB(38, 198, 218);
			} else {
				_node = null;
				Alpha = 0.0f;
			}


		}
	}
}

