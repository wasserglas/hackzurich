﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using DataStructures;

namespace HackZurich
{
	public abstract class TaskView : UIView
	{
		protected nfloat _width;
		protected nfloat _height;

		protected Job _task;
		protected nfloat _padding = 10f;

		private UILabel _titleLabel;
		private UILabel _descLabel;

		protected UIBlurEffect _blur;
		protected UIVisualEffectView _blurView;
		private UIBlurEffectStyle _style;

		public TaskView (CGRect rect, Job task, UIBlurEffectStyle style) : base(rect)
		{
			_width = rect.Width;
			_height = rect.Height;
			_task = task;
			_style = style;
			InitView ();
			ExtendView ();
			ReloadData (_task);
		}

		private void InitView() {
			Layer.CornerRadius = 7f;
			ClipsToBounds = true;

			_blur = UIBlurEffect.FromStyle (_style);
			_blurView = new UIVisualEffectView (_blur) {
				Frame = new RectangleF (0f, 0f, (float)_width, (float) _height)
			};

			AddSubview (_blurView);


			_titleLabel = new UILabel (new CGRect (_padding + 0, _padding + 0, _width - 2 * _padding, 20));
			_titleLabel.Font = UIFont.BoldSystemFontOfSize (20);
			_titleLabel.TextColor = UIColor.White;
			AddSubview (_titleLabel);

			_descLabel = new UILabel (new CGRect (_padding + 0, _titleLabel.Frame.Bottom, _width - 2 * _padding, 20));
			_descLabel.Font = UIFont.SystemFontOfSize (15);
			_descLabel.TextColor = UIColor.White;
			AddSubview (_descLabel);

		}

		public void ReloadData(Job task) {

			_titleLabel.Text = task.Title;
			_descLabel.Text = task.Description;
		}

		protected abstract void ExtendView();

		protected void ChangeTitleColor(UIColor color) {
			_titleLabel.TextColor = color;
		}		

		protected void ChangeDescriptionColor(UIColor color) {
			_descLabel.TextColor = color;
		}
	}
}

