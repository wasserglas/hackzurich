﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using Foundation;

namespace HackZurich
{
	public class UIChatBox : UIView
	{
		public UIChatBox (CGRect rect) : base(rect) 
		{
			BackgroundColor = UIColor.Clear;
			_scrollView = new UIScrollView (new CGRect(0, 70, rect.Width, rect.Height - 70));
			Add (_scrollView);
		}

		float textHeight;
		UIScrollView _scrollView;

		private int index = 0;
		public void AddBubble(string text, bool selfIsSender) {
			CGRect size = ResizeHeigthWithText (text, 0.7f * (float)Frame.Width - 20);
			UIChatBubble bubble = new UIChatBubble(new CGRect(0, textHeight + 10, 0.7 * Frame.Width, size.Height + 20), size, text, selfIsSender);
			if (selfIsSender) {
				bubble.Frame = new CGRect (0.3 * Frame.Width, textHeight + 10, 0.7 * Frame.Width, size.Height + 20);
			}
			bubble.Alpha = 0.0f;
			_scrollView.AddSubview (bubble);
			textHeight += (float) size.Height + 20 + 10;
			_scrollView.ContentSize = new CGSize (Frame.Width, textHeight);
			UIView.Animate (0.3, index * 0.02, UIViewAnimationOptions.AllowUserInteraction, () => {
				bubble.Alpha = 1.0f;
			}, () => {});
			index++;

		}

		public void Flush() {
			index = 0;
			foreach (var v in _scrollView.Subviews) {
				v.RemoveFromSuperview ();
			}
			textHeight = 0;
		}

		public CGRect ResizeHeigthWithText(string text, float width, float maxHeight = 960f) 
		{
			UILabel label = new UILabel();
			label.Text = text;
			label.AdjustsFontSizeToFitWidth = false;
			label.Lines = 0;
			CGSize size = ((NSString)label.Text).StringSize(label.Font,  
				constrainedToSize:new SizeF(width,maxHeight) ,lineBreakMode:UILineBreakMode.WordWrap);

			var labelFrame = label.Frame;
			labelFrame.Size = new SizeF(width, (float)size.Height);
			label.Frame = labelFrame; 
			return labelFrame;
		}
	}
}

